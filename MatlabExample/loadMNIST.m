function [test_i, test_l, train_i, train_l] = loadMNIST(path, filenames)
%
% function [test_i, test_l, train_i, train_l] = loadMNIST(path)
%
% description: Loads MNIST Data as downloaded from:
% http://yann.lecun.com/exdb/mnist/
%
% Input:
%  path      - path do directory containing files
%  filenames - array of filenames, optional should be same order as outputs
% Output:
%  test_i  - test images (ex: 28x28x1000 grayscale image array)
%  test_l  - test labels (ex: 1x1000 labels, 0-9)
%  train_i - training images (ex: 28x28x6000 grayscale image array)
%  train_l - training labels (ex: 1x6000 labels, 0-9)
%

if nargin() == 1
    filenames = {'t10k-images.idx3-ubyte', 't10k-labels.idx1-ubyte', 'train-images.idx3-ubyte', 'train-labels.idx1-ubyte'};
end

use_swap = 0;
test_i = 0;
test_l = 0;
train_i = 0;
train_l = 0;

f_tst_i = fopen([path, '/', filenames{1}]);
f_tst_l = fopen([path, '/', filenames{2}]);
f_trn_i = fopen([path, '/', filenames{3}]);
f_trn_l = fopen([path, '/', filenames{4}]);

d_tst_i = fread(f_tst_i);
d_tst_l = fread(f_tst_l);
d_trn_i = fread(f_trn_i);
d_trn_l = fread(f_trn_l);

mn = typecast(uint8(d_tst_i(1:4)), 'int32');
if mn ~= 2051
    if swapbytes(mn) == 2051
        use_swap = 1;
    else
        fprintf('incorrect test image file');
        return;
    end
end

n = typecast(uint8(d_tst_i(5:8)), 'int32');
r = typecast(uint8(d_tst_i(9:12)), 'int32');
c = typecast(uint8(d_tst_i(13:16)), 'int32');

if use_swap
    n = swapbytes(n);
    r = swapbytes(r);
    c = swapbytes(c);
end

test_i = uint8(zeros(r, c, n));

for i = 1:n
    for j = 1:r
        for k = 1:c
            test_i(j, k, i) = uint8(d_tst_i(16 + (i-1)*r*c + (j-1)*c + k));
        end
    end
end

use_swap = 0;
mn = typecast(uint8(d_tst_l(1:4)), 'int32');
if mn ~= 2049
    if swapbytes(mn) == 2049
        use_swap = 1;
    else
        fprintf('incorrect test label file');
        return;
    end
end

n = typecast(uint8(d_tst_l(5:8)), 'int32');

if use_swap
    n = swapbytes(n);
end

test_l = zeros(n, 1);

for i = 1:n
   test_l(i) = d_tst_l(8 + i); 
end

use_swap = 0;
mn = typecast(uint8(d_trn_i(1:4)), 'int32');
if mn ~= 2051
    if swapbytes(mn) == 2051
        use_swap = 1;
    else
        fprintf('incorrect training image file');
        return;
    end
end

n = typecast(uint8(d_trn_i(5:8)), 'int32');
r = typecast(uint8(d_trn_i(9:12)), 'int32');
c = typecast(uint8(d_trn_i(13:16)), 'int32');

if use_swap
    n = swapbytes(n);
    r = swapbytes(r);
    c = swapbytes(c);
end

train_i = uint8(zeros(r, c, n));

for i = 1:n
    for j = 1:r
        for k = 1:c
            train_i(j, k, i) = uint8(d_trn_i(16 + (i-1)*r*c + (j-1)*c + k));
        end
    end
end

use_swap = 0;
mn = typecast(uint8(d_trn_l(1:4)), 'int32');
if mn ~= 2049
    if swapbytes(mn) == 2049
        use_swap = 1;
    else
        fprintf('incorrect test label file');
        return;
    end
end

n = typecast(uint8(d_trn_l(5:8)), 'int32');

if use_swap
    n = swapbytes(n);
end

train_l = zeros(n, 1);

for i = 1:n
   train_l(i) = d_trn_l(8 + i); 
end
