'''
PythonExample_plotResults.py plots the outputs from the PythonExample.py
script.
'''

import numpy as np
import matplotlib.pyplot as plt
import pickle

(x_test, encoded_imgs, decoded_imgs) = pickle.load(open("data.p", "wb"))

n = 10
plt.figure(figsize=(n*2, 4))
for i in range(n):
	ax = plt.subplot(2, n, i+1)
	plt.imshow(x_test[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
	
	ax = plt.subplot(2, n, i+1+n)
	plt.imshow(decoded_imgs[i].reshape(28, 28))
	plt.gray()
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
	
plt.show()

