#!/bin/bash

# set output and error output filenames, %j will be replaced by Slurm with the jobid
#SBATCH -o PythonExample_%j.out
#SBATCH -e PythonExample_%j.err 

# email me when job starts and stops
#SBATCH --mail-type=ALL
#SBATCH --mail-user=<your email here>

# set up which queue should be joined
#SBATCH -N 1
#SBATCH -p debug

# set the correct directory
#SBATCH -D /home/<username>/ssl_colonialoneexample/PythonExample

# name job
#SBATCH -J PythonExample

# set time limit
#SBATCH -t 02:00:00

# load necesary modules
module load anaconda/4.2.0

# run task
python PythonExample.py

