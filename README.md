## [GWU Colonial One](https://colonialone.gwu.edu/) Tutorial

This repo is meant as an example for running projects on the Colonial One computing cluster at the George Washington University. The examples presented in this example show how to train autoencoders designed in both Matlab and Python3 (using the keras library) since the Smart Systems Lab primarily uses Colonial One to train Neural Networks.

## Logging into Colonial one

You need to have a terminal emulator. 
This alredy exists on MacOS/Linux but you can install [Putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) on Windows.

The first thing to do is to log into a login shell in Colonial One. To do this on a Linux machine, make sure you have an ssh client installed, then run

~~~
ssh <username>@login.colonialone.gwu.edu
~~~

where `<username>` is your GWU Net ID. 
The part of your email before the `@`.

## Clone the repo

Once this is done, you will need to clone this repo. Navigate to where you want to download this repo, then you will need to load the git module, and clone the repo with the following commands:

~~~
moudle load git
git clone https://bitbucket.org/LikeSmith/ssl_colonialoneexample.git
~~~

You have to examples given in the directories:

1. `./MatlabExample`
2. `./PythonExample`

